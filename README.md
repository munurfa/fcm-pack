# Laravel FCM Notification

Laravel package for easy integration of Firebase Cloud Messaging (FCM) with Laravel's notifications.

## Installation
Install the package via `Composer`
```
composer require frengky/fcm-notification
```
Then publish the configuration files to your `app/config` and run the migration
```
php artisan vendor:publish --tag=config
php artisan migrate
```

## Database

By default, there will be a new table `fcm_tokens` automatically created and managed by this package.

|id      | notifiable_type  | notifiable_id | token         | platform |
|:------:|:---------------:|:--------------:|:-------------:|:--------:|
| (uuid) | Your\Model       | (id)          | (token)       | 0        | 

I'm using [Polymorphic Relations](https://laravel.com/docs/eloquent-relationships#polymorphic-relations) to store multiple token for each notifiable with their optional platform id.

value| platform   
:---:|:---:|
 0 | Universal 
 1 | Android 
 2 | APNs 
 3 | Web Push 

This value will determine how the extended message will be constructed before sending to the FCM v1 endpoint

## Getting started

Modify your notifiable class, usually will be the `User` class
```php
use Illuminate\Notifications\Notifiable;
use Frengky\FcmNotification\Concerns\HasFcmNotifications;

class User extends Authenticatable
{
    use Notifiable, HasFcmNotifications;
}
```
Configure your [notification classes](https://laravel.com/docs/notifications#creating-notifications), and add an [additional channel](https://laravel.com/docs/notifications#specifying-delivery-channels) along with another channel that you may already have.
```php
use Illuminate\Notifications\Notification;
use Frengky\FcmNotification\FcmChannel;

class RegisterCompleted extends Notification
{
    public function via($notifiable)
    {
        return [ FcmChannel::class, 'database', 'email' ];
    }
    
    // Define the following methods for the FcmChannel    
    public function toFcm($notifiable)
    {
        return (new FcmMessage())
            ->setTitle('Welcome')
            ->setBody('We are happy to see you')
            ->setData([ 'another' => 'data' ]);
    }
}
``` 
You may need to manage your user's device token, so
```php
// Store a device token for this user
$user->createFcmToken($token); 

// Optional, if you know their platform
$user->createFcmToken($token, FcmPlatform::ANDROID); 

$user->subscribeFcm($topic); // Subscribe this user to a topic
$user->unsubscribeFcm($topic); // Unsubscribe this user from a topic
$user->deleteFcmTokens($token); // Delete some token if you wish
$user->clearFcmTokens(); // Clear the stored token
```

After the user have some device tokens stored, finally, send your notification as usual
```php
$user->notify(new RegisterCompleted);
```
This notification channel will trigger the following events during the process
```php
Illuminate\Notifications\Events\NotificationSending;
Illuminate\Notifications\Events\NotificationSent;
Illuminate\Notifications\Events\NotificationFailed;
```
But, the invalid tokens will be deleted automatically according to the response, not on every `NotificationFailed` event.
