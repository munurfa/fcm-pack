<?php

namespace Frengky\FcmNotification\Tests\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Frengky\FcmNotification\Concerns\HasFcmNotifications;

class User extends Model
{
    use SoftDeletes, Notifiable, HasFcmNotifications;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}