<?php

namespace Frengky\FcmNotification\Tests;

use Frengky\FcmNotification\FcmPlatform;
use Frengky\FcmNotification\Tests\Model\User;
use Frengky\FcmNotification\Tests\Notifications\Welcome;
use Illuminate\Notifications\Events\NotificationFailed;
use Illuminate\Notifications\Events\NotificationSending;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Support\Facades\Event;
use Notification;

class FcmNotificationTests extends TestCase
{
    /**
     * Setup the test environment
     */
    protected function setUp()
    {
        parent::setUp();

        // Create test user
        factory(User::class, 1)->create();
    }

    public function testFcmConfig()
    {
        // Make sure we can auto discover the json key file
        $this->assertNotEmpty(file_exists(env('FIREBASE_CREDENTIALS')));

        // Make sure you prepare those tokens for tests
        $this->assertNotEmpty(env('VALID_TOKEN'));
        $this->assertNotEmpty(env('EXPIRED_TOKEN'));
    }

    public function testCreateUpdateDeleteToken()
    {
        $user = User::find(1);

        $universalToken = $user->createFcmToken('uni-token');
        $this->assertNotEmpty($universalToken);
        $this->assertDatabaseHas('fcm_tokens', [ 'token' => 'uni-token', 'platform' => FcmPlatform::UNIVERSAL ]);

        $apnsToken = $user->createFcmToken('apns-token', FcmPlatform::APNS);
        $this->assertNotEmpty($apnsToken);
        $this->assertDatabaseHas('fcm_tokens', [ 'token' => 'apns-token', 'platform' => FcmPlatform::APNS ]);

        $this->assertCount(2, $user->fcmtokens()->get());

        $user->updateFcmToken('apns-token', 'apns-new-token');
        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => 'apns-token', 'platform' => FcmPlatform::APNS ]);
        $this->assertDatabaseHas('fcm_tokens', [ 'token' => 'apns-new-token', 'platform' => FcmPlatform::APNS ]);

        $user->deleteFcmTokens('apns-new-token');
        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => 'apns-new-token', 'platform' => FcmPlatform::APNS ]);

        $universalToken->delete();
        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => 'uni-token', 'platform' => FcmPlatform::UNIVERSAL ]);

        $this->assertCount(0, $user->fcmtokens()->get());
    }

    public function testFcmDuplicateTokens()
    {
        $user = User::find(1);
        $validToken = env('VALID_TOKEN');
        $user->createFcmToken($validToken);
        $user->createFcmToken($validToken);

        $this->assertCount(1, $user->fcmtokens);
        $user->clearFcmTokens();
        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => $validToken ]);
    }

    public function testFcmSubscribeUnsubscribe()
    {
        $user = User::find(1);
        $validToken = env('VALID_TOKEN');
        $user->createFcmToken($validToken);

        $this->assertNotEmpty($user->subscribeFcm('test-topic'));
        $this->assertNotEmpty($user->unsubscribeFcm('test-topic'));

        $user->clearFcmTokens();
        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => $validToken ]);
    }

    public function testFcmNotificationSuccess()
    {
        $user = User::find(1);
        $validToken = env('VALID_TOKEN');
        $user->createFcmToken($validToken);

        $this->expectsEvents([
            NotificationSending::class,
            NotificationSent::class
        ]);

        $user->notify(new Welcome([
            'key1' => 'value1',
            'key2' => 'value2'
        ]));

        $this->assertCount(1, $user->unreadNotifications);
        foreach($user->unreadNotifications as $notification) {
            $this->assertEquals('Frengky\FcmNotification\Tests\Notifications\Welcome', $notification->type);
        }

        $user->deleteFcmTokens($validToken);
        $this->assertCount(0, $user->fcmtokens()->get());
    }

    public function testFcmNotificationFailure()
    {
        $user = User::find(1);

        $expiredToken = env('EXPIRED_TOKEN');
        $user->createFcmToken($expiredToken);

        $this->expectsEvents([
            NotificationFailed::class
        ]);

        $user->notify(new Welcome([
            'key1' => 'value1',
            'key2' => 'value2'
        ]));

        $this->assertDatabaseMissing('fcm_tokens', [ 'token' => $expiredToken ]);
    }
}