<?php

namespace Frengky\FcmNotification\Tests;

use Frengky\FcmNotification\FcmNotificationServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Setup the test environment
     */
    protected function setUp()
    {
        parent::setUp();

        $this->loadMigrationsFrom(
            realpath(__DIR__.'/database/migrations')
        );
        $this->withFactories(
            realpath(__DIR__.'/database/factories')
        );

        $this->artisan('migrate', ['--database' => 'testing']);
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
        $app['config']->set('fcm.firebase_credentials', ''); // auto-discover
        $app['config']->set('fcm.android', [
            'ttl' => '3600s',
            'priority' => 'high'
        ]);
    }

    /**
     * Define package service provider
     *
     * @param \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            FcmNotificationServiceProvider::class
        ];
    }

    /**
     * Get application timezone.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return string|null
     */
    protected function getApplicationTimezone($app)
    {
        return 'Asia/Jakarta';
    }
}
