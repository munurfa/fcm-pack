<?php

namespace Frengky\FcmNotification\Tests\Notifications;

use Frengky\FcmNotification\FcmMessage;
use Frengky\FcmNotification\Tests\Model\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

use Frengky\FcmNotification\FcmChannel;

class Welcome extends Notification
{
    use Queueable;

    /** @var array */
    public $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return [ FcmChannel::class, 'database' ];
    }

    /**
     * Get the FCM representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Frengky\FcmNotification\FcmMessage
     */
    public function toFcm($notifiable)
    {
        return (new FcmMessage())
            ->setTitle('Welcome')
            ->setBody('We are happy to see you')
            ->setData($this->data);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }
}