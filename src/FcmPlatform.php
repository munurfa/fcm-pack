<?php

namespace Frengky\FcmNotification;

final class FcmPlatform
{
    /** @var int */
    const UNIVERSAL = 0;

    /** @var int  */
    const ANDROID = 1;

    /** @var int */
    const APNS = 2;

    /** @var int */
    const WEBPUSH = 3;
}