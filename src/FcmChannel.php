<?php

namespace Frengky\FcmNotification;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Events\NotificationSending;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Events\NotificationFailed;
use Kreait\Firebase\Exception\Messaging\NotFound;

use Frengky\FcmNotification\Facades\Fcm;
use Frengky\FcmNotification\FcmPlatform;

class FcmChannel
{
    /**
     * Send the notification to Firebase Cloud Messaging
     *
     * @param \Frengky\FcmNotification\Concerns\HasFcmNotifications $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var Collection $tokens */
        $tokens = $notifiable->routeNotificationFor('fcm', $notification);
        if (empty($tokens)) {
            return;
        }

        /** @var FcmMessage $message */
        $message = $notification->toFcm($notifiable);
        if (! $message instanceof FcmMessage) {
            return;
        }

        foreach ($tokens as $token) {
            event(new NotificationSending($notifiable, $notification, get_class($this)));

            /** @var FcmToken $token */
            if ($token->platform == FcmPlatform::ANDROID) {
                $message = $message->forAndroid();
            } elseif ($token->platform == FcmPlatform::APNS) {
                $message = $message->forApns();
            } elseif ($token->platform == FcmPlatform::WEBPUSH) {
                $message = $message->forWebPush();
            } else {
                $message = $message->forUniversal();
            }

            $response = null;
            try {

                /** @var array $response */
                $response = Fcm::sendToDevice($token->token, $message);
                event(new NotificationSent($notifiable, $notification, get_class($this), $response));

            } catch (NotFound $e) {

                $notifiable->deleteFcmTokens($token->token);
                event(new NotificationFailed($notifiable, $notification, get_class($this)), [
                    'error' => $e->getMessage()
                ]);

            } catch (\Exception $e) {

                report($e);
                event(new NotificationFailed($notifiable, $notification, get_class($this)), [
                    'error' => $e->getMessage()
                ]);

            }
        }
    }
}