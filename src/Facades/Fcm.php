<?php

namespace Frengky\FcmNotification\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Fcm
 *
 * @method static send(\Kreait\Firebase\Messaging\CloudMessage $message)
 * @method static sendToDevice(string $token, \Frengky\FcmNotification\FcmMessage $fcmMessage)
 * @method static sendToTopic(string $topic, \Frengky\FcmNotification\FcmMessage $fcmMessage)
 * @method static subscribe(string $topic, array $tokens)
 * @method static unsubscribe(string $topic, array $tokens)
 * @method static validate(array $messages)
 */
class Fcm extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'fcm';
    }
}