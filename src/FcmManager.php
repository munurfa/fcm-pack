<?php

namespace Frengky\FcmNotification;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\ServiceAccount;

use Illuminate\Support\Str;
use RuntimeException;

class FcmManager
{
    /** @var \Kreait\Firebase  */
    private $firebase;

    /**
     * FcmManager constructor.
     * @param string $credential
     */
    public function __construct($credential)
    {
        if (!empty($credential)) {
            if (! file_exists(realpath($credential))) {
                throw new RuntimeException('Missing firebase credentials (service account) JSON key file.');
            }
            $serviceAccount = ServiceAccount::fromJsonFile($credential);
        } else {
            $serviceAccount = ServiceAccount::discover();
        }

        $this->firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
    }

    /**
     * Send a cloud message
     *
     * @param CloudMessage $message
     * @return array
     * @throws \Kreait\Firebase\Exception\Messaging\NotFound|\Exception
     */
    public function send(CloudMessage $message)
    {
        return $this->firebase->getMessaging()->send($message);
    }

    /**
     * Send a message to a device
     *
     * @param string $token
     * @param FcmMessage $fcmMessage
     * @return array
     * @throws \Kreait\Firebase\Exception\Messaging\NotFound|\Exception
     */
    public function sendToDevice($token, FcmMessage $fcmMessage)
    {
        return $this->send($fcmMessage->toCloudMessage('token', $token));
    }

    /**
     * Send a message (with or without condition) to a topic
     *
     * @param string $topic
     * @param FcmMessage $fcmMessage
     * @return array
     * @throws \Kreait\Firebase\Exception\Messaging\NotFound|\Exception
     */
    public function sendToTopic($topic, FcmMessage $fcmMessage)
    {
        $type = preg_match('/[^a-zA-Z0-9\s]+/', $topic) ? 'condition' : 'topic';
        return $this->send($fcmMessage->toCloudMessage($type, $topic));
    }

    /**
     * Subscribe to a topic
     *
     * @param string $topic
     * @param array $tokens
     * @return array
     */
    public function subscribe($topic, $tokens)
    {
        return $this->firebase->getMessaging()->subscribeToTopic($topic, $tokens);
    }

    /**
     * Unsubscribe from a topic
     *
     * @param string $topic
     * @param array $tokens
     * @return array
     */
    public function unsubscribe($topic, $tokens)
    {
        return $this->firebase->getMessaging()->unsubscribeFromTopic($topic, $tokens);
    }

    /**
     * Validate a message
     *
     * @param array|CloudMessage|Message $messages
     * @return array
     *
     * @throws \Kreait\Firebase\Exception\Messaging\InvalidArgumentException
     * @throws \Kreait\Firebase\Exception\Messaging\InvalidMessage
     */
    public function validate($messages)
    {
        return $this->firebase->getMessaging()->validate($messages);
    }
}