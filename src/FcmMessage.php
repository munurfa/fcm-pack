<?php

namespace Frengky\FcmNotification;

use Frengky\FcmNotification\Concerns\HasAndroid;
use Frengky\FcmNotification\Concerns\HasApns;
use Frengky\FcmNotification\Concerns\HasWebPush;

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class FcmMessage
{
    use HasAndroid, HasApns, HasWebPush;

    /** @var string */
    protected $title;

    /** @var string */
    protected $body;

    /** @var array */
    protected $data;

    /** @var bool */
    protected $hasAndroid = false;

    /** @var bool */
    protected $hasApns = false;

    /** @var bool */
    protected $hasWebPush = false;

    /**
     * FcmMessage constructor.
     */
    public function __construct()
    {
    }

    /**
     *  Title must be present on android notification and ios (watch) notification.
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Indicates notification body text.
     *
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Set data payload attached to a message,
     * must be an array of key-value pairs where all keys and values are strings.
     *
     * @param array $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Indicating this message include Android
     * @return $this
     */
    public function forAndroid()
    {
        $this->hasAndroid = true;
        return $this;
    }

    /**
     * Indicating this message include iOS
     * @return $this
     */
    public function forApns()
    {
        $this->hasApns = true;
        return $this;
    }

    /**
     * Indicating this message include WebPush
     * @return $this
     */
    public function forWebPush()
    {
        $this->hasWebPush = true;
        return $this;
    }

    /**
     * Indicating this message include all platform specific message
     *
     * @return $this
     */
    public function forUniversal()
    {
        $this->hasAndroid = true;
        $this->hasApns = true;
        $this->hasWebPush = true;
        return $this;
    }

    /**
     * Build the CloudMessage instance
     *
     * @param string $type
     * @param string $value
     * @return CloudMessage
     */
    public function toCloudMessage($type, $value)
    {
        $message = CloudMessage::withTarget($type, $value)
            ->withNotification(Notification::create($this->title, $this->body))
            ->withData($this->data);

        if ($this->hasAndroid) {
            $message = $message->withAndroidConfig($this->buildAndroidConfig());
        }
        if ($this->hasApns) {
            $message = $message->withApnsConfig($this->buildApnsConfig());
        }
        if ($this->hasWebPush) {
            $message = $message->withWebPushConfig($this->buildWebPushConfig());
        }

        return $message;
    }
}