<?php

namespace Frengky\FcmNotification;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class FcmNotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadMigrationsFrom(
            realpath(__DIR__ . '/../database/migrations')
        );

        $this->publishes([
            realpath(__DIR__.'/../config/fcm.php') => config_path('fcm.php')
        ], 'config');
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(
            realpath(__DIR__.'/../config/fcm.php'), 'fcm'
        );

        $this->app->singleton('fcm', function() {
            return new FcmManager($this->getFirebaseCredential());
        });
    }

    /**
     * Return firebase credentials (service account json key) file path from config
     *
     * @return string
     */
    protected function getFirebaseCredential()
    {
        return $this->app['config']['fcm.firebase_credentials'];
    }
}