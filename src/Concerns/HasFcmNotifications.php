<?php

namespace Frengky\FcmNotification\Concerns;

use Frengky\FcmNotification\FcmPlatform;
use Frengky\FcmNotification\FcmToken;
use Frengky\FcmNotification\Facades\Fcm;

use Illuminate\Database\Eloquent\Collection;

trait HasFcmNotifications
{
    /**
     * Register deleted event on the entity to
     * delete all tokens when entity has been deleted
     */
    public static function bootHasFcmNotifications()
    {
        static::deleted(function ($entity) {
            /** @var HasFcmNotifications $entity */
            $entity->clearFcmTokens();
        });
    }

    /**
     * Get the entity's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fcmtokens()
    {
        return $this->morphMany(FcmToken::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    /**
     * Route notifications for the fcm channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return Collection
     */
    public function routeNotificationForFcm()
    {
        return $this->fcmtokens;
    }

    /**
     * Create new token for this notifiable
     *
     * @param string $token
     * @param int $platform
     * @return FcmToken
     */
    public function createFcmToken($token, $platform = FcmPlatform::UNIVERSAL)
    {
        $current = $this->fcmtokens()->where('token', $token)->first();
        if (empty($current)) {
            return $this->fcmtokens()->create([
                'token' => $token,
                'platform' => $platform
            ]);
        }
        return $current;
    }

    /**
     * Update token for this notifiable
     *
     * @param string $oldToken
     * @param string $newToken
     * @return FcmToken
     */
    public function updateFcmToken($oldToken, $newToken)
    {
        $current = $this->fcmtokens()->where('token', $oldToken)->first();
        if ($current) {
            $current->update([ 'token' => $newToken ]);
        }
        return $current;
    }

    /**
     * Delete tokens for this notifiable
     *
     * @param mixed $token
     */
    public function deleteFcmTokens($token)
    {
        $this->fcmtokens()->whereIn('token',
            is_array($token) ? $token : [$token])->delete();
    }

    /**
     * Delete all tokens for this notifiable
     */
    public function clearFcmTokens()
    {
        $this->fcmtokens()->delete();
    }

    /**
     * Subscribe this notifiable to a topic
     *
     * @param string $topic
     * @return array
     */
    public function subscribeFcm($topic)
    {
        $tokens = collect($this->fcmtokens()->get())->pluck('token')->all();
        return Fcm::subscribe($topic, $tokens);
    }

    /**
     * Unsubscribe this notifiable from a topic
     *
     * @param $topic
     * @return array
     */
    public function unsubscribeFcm($topic)
    {
        $tokens = collect($this->fcmtokens()->get())->pluck('token')->all();
        return Fcm::unsubscribe($topic, $tokens);
    }
}