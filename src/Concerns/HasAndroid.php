<?php

namespace Frengky\FcmNotification\Concerns;

use Kreait\Firebase\Messaging\AndroidConfig;

trait HasAndroid
{
    /** @var string */
    protected $clickAction;

    /** @var string */
    protected $sound = 'default';

    /** @var string */
    protected $tag;

    /**
     * The sound to play when the Android device receives the notification.
     * Supports "default" or the filename of a sound resource bundled in the app. Sound files must reside in /res/raw/
     *
     * @param string $sound
     * @return $this
     */
    public function setSound($sound)
    {
        $this->hasAndroid = true;
        $this->sound = $sound;
        return $this;
    }

  

    /**
     * Indicates the action associated with a user click on the notification.
     * FCM always start launcher activity, if the action is not set.
     *
     * @param string $action
     * @return $this
     */
    public function setClickAction($action)
    {
        $this->hasAndroid = true;
        $this->clickAction = $action;
        return $this;
    }

    /**
     * Identifier used to replace existing notifications in the notification drawer.
     * If not specified, each request creates a new notification.
     * If specified and a notification with the same tag is already being shown,
     * the new notification replaces the existing one in the notification drawer.
     *
     * @param string $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->hasAndroid = true;
        $this->tag = $tag;
        return $this;
    }

    /**
     * Build the AndroidConfig instance
     *
     * @return AndroidConfig
     */
    protected function buildAndroidConfig()
    {
        return AndroidConfig::fromArray([
            'ttl' => config('fcm.android.ttl'),
            'priority' => config('fcm.android.priority'),
            'notification' => array_filter([
                'title' => $this->title,
                'body' => $this->body,
                'sound' => $this->sound,
                'clickAction' => $this->clickAction,
                'tag' => $this->tag
            ])
        ]);
    }
}