<?php

namespace Frengky\FcmNotification\Concerns;

use Kreait\Firebase\Messaging\WebPushConfig;

trait HasWebPush
{
    /** @var string */
    protected $webPushIcon;
    protected $webPushLink;

    /**
     * Set full url to the icon
     *
     * @param string $icon
     * @return $this
     */
    public function setWebPushIcon($icon)
    {
        $this->hasWebPush = true;
        $this->webPushIcon = $icon;
        return $this;
    }

    public function setWebPushLink($link)
    {
        $this->hasWebPush = true;
        $this->webPushLink = $link;
        return $this;
    }

    /**
     * Build the WebPushConfig
     *
     * @return WebPushConfig
     */
    protected function buildWebPushConfig()
    {
        return WebPushConfig::fromArray([
            'notification' => array_filter([
                'title' => $this->title,
                'body' => $this->body,
                'icon' => $this->webPushIcon
            ]),
            'fcm_options'=> array_filter([
                'link' => $this->webPushLink
            ])
        ]);
    }
}