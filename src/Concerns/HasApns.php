<?php

namespace Frengky\FcmNotification\Concerns;

use Kreait\Firebase\Messaging\ApnsConfig;

trait HasApns
{
    /** @var int */
    protected $badge = 0;

    /**
     * Indicates the badge on the client app home icon. (iOS only)
     *
     * @param int $badge
     * @return $this
     */
    public function setBadge($badge)
    {
        $this->hasApns = true;
        $this->badge = $badge;
        return $this;
    }

    /**
     * Build the ApnsConfig
     *
     * @return ApnsConfig
     */
    protected function buildApnsConfig()
    {
        $aps = [
            'alert' => [
                'title' => $this->title,
                'body' => $this->body
            ]
        ];
        if ($this->badge) {
            $aps['badge'] = $this->badge;
        }

        return ApnsConfig::fromArray([
            'headers' => config('fcm.apns.headers'),
            'payload' => [
                'aps' => $aps
            ]
        ]);
    }
}