<?php

namespace Frengky\FcmNotification;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;


/**
 * Class FcmToken
 *
 * @property string $id
 * @property string $notifiable_type
 * @property int $notifiable_id
 * @property string $token
 * @property int $platform
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class FcmToken extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fcm_tokens';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Override the boot to register creating event
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($token) {
            $token->id = Uuid::uuid4()->toString();
            return true;
        });
    }

    /**
     * Get the notifiable entity that the upload belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function notifiable()
    {
        return $this->morphTo();
    }
}